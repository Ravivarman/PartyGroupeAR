import { Component } from '@angular/core';
import { NavController,Platform, NavParams  } from 'ionic-angular';
import { Contacts } from 'ionic-native';


@Component({
  templateUrl: 'navigation-details.html' 
})
export class BasicPage {

  contactlist: any;
  constructor(params: NavParams, public navCtrl: NavController,public platform: Platform) {
this.platform.ready().then(() => {
       var opts = {  
          filter : "M",                                
         multiple: true,        
         hasPhoneNumber:true,                            
          fields:  [ 'displayName', 'name' ]
        };
        Contacts.find([ 'displayName', 'name' ],opts).then((contacts) => {
          this.contactlist=contacts;
        }, (error) => {
          console.log(error);
        })
    })

 }
  }
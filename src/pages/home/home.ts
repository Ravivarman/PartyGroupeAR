import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddEventPage } from '../add-event/add-event';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  }

   /*var nativeHomeInputBox = document.getElementById('txtHome').getElementsByTagName('input')[0];
   let autocomplete1 = new google.maps.places.Autocomplete(nativeHomeInputBox, searchOptions);*/

  openAddPage() {
    this.navCtrl.push(AddEventPage)
  }

}